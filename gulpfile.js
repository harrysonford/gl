require('./.env');

var { Gitlab } = require('gitlab');
var gulp = require('gulp');
var file = require('gulp-file');

var gitLabHost = env.gitLabHost;
var userToken = env.userToken;
var useIssueCaptionInBranchName = env.useIssueCaptionInBranchName;

var projectIdApp = 42;
var projectIdApi = 41;
var projectIdWeb = 35;

var memberIdHarry = 58;
var memberIdJerry = 57;
var memberIdMatoB = 2;
var memberIdDusanM = 29;
var memberIdDusanS = 3;
var memberIdPeterS = 4;
var memberIdAndrej = 48;
var memberIdNorbi = 52;

var weekday = new Array(7);
weekday[0] = "Sunday";
weekday[1] = "Monday";
weekday[2] = "Tuesday";
weekday[3] = "Wednesday";
weekday[4] = "Thursday";
weekday[5] = "Friday";
weekday[6] = "Saturday";

var api = new Gitlab({
	host: gitLabHost,
	token: userToken,
});

// project
// api.Projects.show(42).then(projects => {
// 	console.log(projects);
// });

// project members
// api.ProjectMembers.all(42).then(projects => {
// 	console.log(projects);
// });

// project labels
// api.Labels.all(35).then(labels => {
// 	console.log(labels);
// });


function commatize(str) {
	str = str.replace(/[`~!@$%^&*()|+\=?;:'",.<>\{\}\[\]\\\/]/gi, '');
	str = str.replace(/\ \ /g, '');
	str = str.split(' ').join('-');
	str = str.substring(0, 40);
	return str.toLowerCase();
}

/**

	gulp issue --title "hhdsd sd sds dsdsd sd"
	creates issue in tessweb + creates branch in tessapp from master + creates mr for the branch to stage-test

	gulp issue --id 3137
	for given issue from tessweb creates branch in tessapp from master + creates mr for the branch to stage-test

	gulp issue --done 3137
	for given issue merges merge request, set assignee to Andrea, set labels

 */

async function setMilestone(projectId, issueId, milestoneName) {
	if (milestoneName) {
		var foundMilestone = 0;
		var milestones = await api.ProjectMilestones.all(projectId);
		foundMilestone = milestones.find((milestone) => milestone.title === milestoneName);
		if (foundMilestone) {
			api.Issues.edit(projectId, issueId, {
				'milestone_id': foundMilestone.id
			})
		}
	}
}

async function createFeatureBranch(projectId, sourceBranch, issueId, issueTitle) {
	var featureBranchName = 'dusan/TESS_Web#' + issueId + ((useIssueCaptionInBranchName) ? ('-' + commatize(issueTitle)) : '');
	// store the branch name to file to read it further in the script

	console.log(projectId, featureBranchName, sourceBranch);

	var br = await api.Branches.create(projectId, featureBranchName, sourceBranch);
	console.log('feature branch created ' + featureBranchName);
}

async function recordToFileFeatureBranch(issueId, issueTitle) {
	var featureBranchName = 'dusan/TESS_Web#' + issueId + ((useIssueCaptionInBranchName) ? ('-' + commatize(issueTitle)) : '');
	// store the branch name to file to read it further in the script
	var result = await file('result.txt', featureBranchName, { src: true }).pipe(gulp.dest('.'));
}


async function createMergeRequest(projectId, destinationBranch, issueId, issueTitle) {
	var featureBranchName = 'dusan/TESS_Web#' + issueId + ((useIssueCaptionInBranchName) ? ('-' + commatize(issueTitle)) : '');
	var mr = await api.MergeRequests.create(projectId, featureBranchName, destinationBranch, featureBranchName);
	console.log('mr created');
}

async function getIssueTitle(projectId, issueId) {
	let issue = await api.Issues.show(projectId, issueId);
	if (!issue) {
		console.log('given issue id doesn\'t exist');
		return '';
	}
	console.log('issue found - ' + issueId + ' - ' + issue.web_url);
	return issue.title;
}

async function createIssue(projectId, issueTitle, issuePrefix) {
	let issue = await api.Issues.create(projectId, {
		title: issuePrefix + ' ' + issueTitle
	});
	console.log('new issue created - ' + issue.iid + ' - ' + issue.web_url);
	return { id: issue.iid, title: issue.title };
}

async function editIssue(projectId, issueId, issueLabels, issueAssignee) {
	let issue = await api.Issues.edit(projectId, issueId, {
		// 'labels': issueLabels,
		'assignee_id': issueAssignee
	})
}

function resolveParam(paramName) {
	var paramIndex = process.argv.indexOf("--" + paramName);
	var param = '';
	if (paramIndex !== -1) {
		param = process.argv[paramIndex + 1];
	}
	return param;
}

gulp.task('issue', async function () {
	var issueId = resolveParam('id');
	var issueTitle = resolveParam('title');
	var issueDoneId = resolveParam('done');

	if (issueDoneId) {
		issueTitle = await getIssueTitle(projectIdWeb, issueDoneId);
		recordToFileFeatureBranch(issueDoneId, issueTitle);
		editIssue(projectIdWeb, issueDoneId, "stat: Testing,topic: Mobile Application", env.tester);

	} else if (issueId) {
		issueTitle = await getIssueTitle(projectIdWeb, issueId);
		editIssue(projectIdWeb, issueId, env.labels, env.developer);
		setMilestone(projectIdWeb, issueId, env.milestone);
		createFeatureBranch(env.defaultProjectForBranch, env.sourceBranch, issueId, issueTitle);
		recordToFileFeatureBranch(issueId, issueTitle);
		createMergeRequest(env.defaultProjectForBranch, env.destinationBranch, issueId, issueTitle);

	} else if (issueTitle) {
		result = await createIssue(projectIdWeb, issueTitle, env.issuePrefix);
		issueId = result.id;
		issueTitle = result.title;
		editIssue(projectIdWeb, issueId, env.labels, env.developer);
		setMilestone(projectIdWeb, issueId, env.milestone);
		createFeatureBranch(env.defaultProjectForBranch, env.sourceBranch, issueId, issueTitle);
		recordToFileFeatureBranch(issueId, issueTitle);
		createMergeRequest(env.defaultProjectForBranch, env.destinationBranch, issueId, issueTitle);

	}

	return Promise.resolve('done');
})

gulp.task('hours', async function () {
	var issueIdIndex = process.argv.indexOf("--id");
	var issueId = '';
	if (issueIdIndex !== -1) {
		issueId = process.argv[issueIdIndex + 1];
	}

	var hoursIndex = process.argv.indexOf("--hours");
	var hours = '';
	if (hoursIndex !== -1) {
		hours = process.argv[hoursIndex + 1];
	}

	var captionIndex = process.argv.indexOf("--caption");
	var caption = '';
	if (captionIndex !== -1) {
		caption = process.argv[captionIndex + 1];
	}

	if (!issueId) {
		console.log('issue not found');
	}

	if (!hours) {
		console.log('hours not given');
	}

	if (caption) {
		var today = new Date();
		var note = await api.IssueNotes.create(projectIdWeb, issueId, today.toISOString().slice(0, 10) + " - " + weekday[today.getDay()] + " - " + caption);
	}
	var spendTimeResult = await api.Issues.addSpentTime(projectIdWeb, issueId, hours);

	return Promise.resolve('done');
})