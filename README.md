# Possible use cases from command line

**issue title "hhdsd sd sds dsdsd sd"**
> creates issue in tessweb + creates branch in tessapp from stage-test + creates mr for the branch  
> it sets the issue assignee from env file  
> it sets the issue label to in progress  
> it sets the issue milestone  
> if launched from within the git dir it checkouts the created feature branch locally  

**issue id 3137**
> for given existing issue from tessweb creates branch in tessapp from stage-test + creates mr for the branch  
> it sets the issue assignee from env file  
> it sets the issue label to in progress  
> it sets the issue milestone  
> if launched from within the git dir it checkouts the created feature branch locally

**hours 3137 1.5**
> tracks 1.5 hours on the issue 3137

**hours 3137 1.5 "i did something very important"**
> tracks 1.5 hours on the issue 3137  
> plus it adds a note  

# Installation

1. clone project into a separate directory

1. install nodejs

1. in root of the project launch ```npm install```

1. add directory to the path so it can be launched from anywhere

1. get your gitlab access token (it is possible to get one in gitlab -> user settings -> access token)

1. create .env file based on the .env.template file  
and set your token in .env file -> userToken = 'xxxxxxxxxxxxxxxxxxxx'  
also set other customizations  

1. set other customizations ...  
.env.template contains 3 possible configurations  
I) app developer  
II) for api developer  
III) tesweb developer  
```
   env = {
    'gitLabHost': 'https://gitlab.svarba.sk',
	'userToken': '-------------',
	// created issue will be assigned or existing issue reasigned to this assignee
    'defaultAssignee': 58,
    // feature branch will be created from this branch
	'sourceBranch': 'master',
    // merge request will be created against destinationBranch destination
	'destinationBranch': 'stage-test',
    // project for the feature branch 
	'defaultProjectForBranch': 41,
    // default milestone
	'milestone': '2019 December',
    // labels separated by comma 
	'labels': 'stat: In Progress,topic: API',
    // newly created issue prefix
	'issuePrefix': 'Api:'
}
```


# Contact
feel free to define new usecases  
contact me at hrecho:)gmail:)com
